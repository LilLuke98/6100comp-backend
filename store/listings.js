const listings = [
  {
    id: 201,
    title: "New Brake Pads (Disks are an extra £80)",
    images: [{ fileName: "BreakPad2" }],
    price: 80,
    categoryId: 5,
    userId: 1,
    location: {
      latitude: 37.78825,
      longitude: -122.4324,
    },
  },
  {
    id: 3,
    title: "New Wheels! CHEAPEST PRICE AROUND",
    description:
      "We will do a deal at £240 for a full set of 4 and check your steering alignment",
    images: [{ fileName: "Alloys1" }],
    categoryId: 1,
    price: 80,
    userId: 2,
    location: {
      latitude: 37.78825,
      longitude: -122.4324,
    },
  },
  {
    id: 1,
    title: "New Exhaust System!",
    description:
      "A new exhaust system will help your car be more enviromentally friendly and reduce your fuel consumption!",
    images: [
      { fileName: "Exhaust" }
    ],
    price: 110,
    categoryId: 1,
    userId: 1,
    location: {
      latitude: 37.78825,
      longitude: -122.4324,
    },
  },
  {
    id: 2,
    title: "MOT check",
    description:
      "We can also complete a full sevice for an extra £80 (not including any repair costs)",
    images: [{ fileName: "MOT3" }],
    categoryId: 5,
    price: 39,
    userId: 2,
    location: {
      latitude: 37.78825,
      longitude: -122.4324,
    },
  },
  {
    id: 102,
    title: "ABS (Antilock breaking system) Check",
    description: "We can check and fix any ABS problems you have!",
    images: [{ fileName: "ABS" }],
    price: 100,
    categoryId: 3,
    userId: 1,
    location: {
      latitude: 37.78825,
      longitude: -122.4324,
    },
  },
  {
    id: 101,
    title: "MOT Test",
    images: [{ fileName: "MOT2" }],
    price: 42,
    categoryId: 3,
    userId: 1,
    location: {
      latitude: 37.78825,
      longitude: -122.4324,
    },
  },
  {
    id: 4,
    title: "Sound System Upgrade",
    description: "We will fit one bass booster and two amplifiers",
    images: [
      { fileName: "SoundSystem1" },
      { fileName: "SoundSystem2" },
    ],
    categoryId: 1,
    price: 250,
    userId: 2,
    location: {
      latitude: 37.78825,
      longitude: -122.4324,
    },
  },
  {
    id: 6,
    title: "Fix any bodywork issues!",
    description: "We can fix any bodywork issues on your car! message us for a price NOW!",
    images: [
      { fileName: "Bodywork1" },
      { fileName: "Bodywork2" },
      { fileName: "Bodywork3" },
    ],
    categoryId: 5,
    price: 50,
    userId: 2,
    location: {
      latitude: 37.78825,
      longitude: -122.4324,
    },
  },
];

const addListing = (listing) => {
  listing.id = listings.length + 1;
  listings.push(listing);
};

const getListings = () => listings;

const getListing = (id) => listings.find((listing) => listing.id === id);

const filterListings = (predicate) => listings.filter(predicate);

module.exports = {
  addListing,
  getListings,
  getListing,
  filterListings,
};
